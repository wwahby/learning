import numpy as np
import random as rand
import matplotlib.pyplot as plt

def CoinTosses(max_tosses, print_count=100):
	num_tosses = 0
	num_heads = 0
	num_tails = 0
	head_ratio = 0

	for ind in range(0,max_tosses):
		val = rand.randint(0,1)
		if val == 1:
			num_heads+=1
		else:
			num_tails+=1
		num_tosses+=1
		head_ratio = num_heads / num_tosses
		fraction_balance = head_ratio - 1/2

		if (num_tosses >= print_count) and (np.mod(ind, print_count) == 0):
			print(fraction_balance)


def CoinTossesVec(max_tosses, print_count=100):
	num_tosses = 0
	num_heads = 0
	num_tails = 0
	head_ratio = 0

	data_vec = np.zeros(max_tosses)
	frac_vec = np.zeros(max_tosses)

	for ind in range(0,max_tosses):
		val = rand.randint(0,1)
		if val == 1:
			num_heads+=1
		else:
			num_tails+=1
		num_tosses+=1
		head_ratio = num_heads / num_tosses
		fraction_balance = head_ratio - 1/2

		data_vec[ind] = val
		frac_vec[ind] = fraction_balance

		if (num_tosses >= print_count) and (np.mod(ind, print_count) == 0):
			print(fraction_balance)


	plt.figure(1)
	plt.plot(frac_vec)
	plt.grid(True)


def plotCoinTosses():
	plt.figure(1)
	plt.clf()
	CoinTossesVec(10)
	CoinTossesVec(100)
	CoinTossesVec(1000)


def roll_dice(num_sides=6, num_times=1):
	count = num_times
	tot = 0
	while count > 0:
		tot += rand.randint(1, num_sides)
		count -= 1

	return tot

def roll_three_dice_for_ratio(num_times = 1):
	num_nines = 0
	num_tens = 0

	for ind in range(num_times):
		val = roll_dice(num_sides=6, num_times=3)
		if val == 9:
			num_nines+= 1
		elif val == 10:
			num_tens += 1


	return num_nines/num_tens


def simple_racquetball_volley(pw):
	val = rand.random()
	outcome = 0
	if val <= pw:
		outcome = 1

	return outcome

def simple_racquetball_game(pw_serve=0.5, pw_rec=0.5, max_score=21):
	#pw_serve = 0.6
	#pw_rec = 0.5
	#max_score = 21

	cur_score_a = 0
	cur_score_b = 0
	# assume start by serving
	pw = pw_serve
	while (cur_score_a < max_score) and (cur_score_b < max_score):
		outcome = simple_racquetball_volley(pw)
		if(outcome == 1):
			pw = pw_serve
			cur_score_a += 1
		else:
			pw = pw_rec
			cur_score_b +=1

	winner = 0
	if cur_score_a == 21:
		winner = 1

	return winner


def run_racquetball_games(num_games):
	num_wins = 0
	pw_serve = 0.6
	pw_rec = 0.5
	max_score = 21
	for ind in range(num_games):
		winner = simple_racquetball_game(pw_serve=pw_serve, pw_rec=pw_rec, max_score=max_score)
		num_wins += winner

	return num_wins / num_games


def roulette_red(num_spins):
	num_vals = 38
	num_green = 2
	num_red = (num_vals - num_green) / 2

	p_red = num_red / num_vals
	winnings=0

	vec = np.zeros(num_spins)
	for ind in range(num_spins):
		val = rand.random()
		if val <= p_red:
			winnings +=1
		else:
			winnings -=1
		vec[ind] = winnings

	return vec


def roulette_num(num_spins):
	num_vals = 38

	winnings=0

	p_win = 1/38
	vec = np.zeros(num_spins)
	for ind in range(num_spins):
		val = rand.random()
		if val <= p_win:
			winnings += 36
		else:
			winnings -=1
		vec[ind] = winnings

	return vec

def compare_roulette(num_spins):
	winnings_red = roulette_red(num_spins)
	winnings_num = roulette_num(num_spins)

	plt.figure(1)
	plt.clf()
	plt.grid(True)
	plt.plot(winnings_red, 'r-')
	plt.plot(winnings_num, 'k-')
	plt.xlabel('Round Number')
	plt.ylabel('Winnings')


def martingale_bet(win, lose, start_bet, p_win):
	won_last = True
	winnings = 0
	bet = start_bet

	winnings_list = []
	while (winnings < win) and (winnings > -lose):
		if won_last:
			bet = start_bet
		else:
			bet = bet*2

		roll = rand.random()
		if roll <= p_win:
			winnings += bet
			won_last = True
		else:
			winnings -= bet
			won_last = False
		winnings_list.append(winnings)

	winnings_vec = np.array(winnings_list)
	return winnings_vec

def many_martingales(num_runs):

	plt.figure(1)
	plt.clf()
	plt.grid(True)
	for ind in range(num_runs):
		winnings_vec = martingale_bet(5,100,1,1/38)
		plt.plot(winnings_vec)


def simple_election_sample(p_rep, p_dem, sample_size):
	#assuming p_rep + p_dem <= 1
	num_rep = 0
	num_dem = 0

	for ind in range(sample_size):
		val = rand.random()
		if val <= p_rep:
			num_rep += 1
		elif val <= (p_rep + p_dem):
			num_dem +=1

	rep_wins = 0
	if num_rep > num_dem:
		rep_wins = 1

	return rep_wins


def many_elections(p_rep, p_dem, sample_size, num_repeats):

	rep_wins = 0
	for ind in range(num_repeats):
		outcome = simple_election_sample(p_rep, p_dem, sample_size)
		rep_wins += outcome

	acc_frac = rep_wins/num_repeats

	return acc_frac

def rand_count(p, num):
	count = 0
	for ind in range(num):
		val = rand.random()
		if val <= p:
			count += 1
	return count


def hospital(num_days):
	p = 0.5
	limit_frac = 0.6

	num_a = 45
	num_b = 15

	limit_days_a = 0
	limit_days_b = 0

	for ind in range(num_days):
		count_a = rand_count(p, num_a)
		count_b = rand_count(p, num_b)

		if (count_a / num_a) > limit_frac:
			limit_days_a += 1

		if (count_b / num_b) > limit_frac:
			limit_days_b += 1

	return limit_days_a / limit_days_b


def count_flips():
	val = 0
	num = 0

	while(val == 0):
		val = rand.randint(0,1)
		num += 1

	return num

def calc_payouts(num_plays):
	payout = 0
	for ind in range(num_plays):
		num = count_flips()
		payout += 2**num

	ev = payout / num_plays
	return ev


def hot_streak(p, num_shots):
	streak = 0
	max_streak = 0
	for ind in range(num_shots):
		val = rand.random()
		if val < p:
			streak += 1
		else:
			if streak > max_streak:
				max_streak = streak
			streak = 0
	return max_streak

def hot_streaks(p, num_shots, streak_min, num_games):
	num_streaks = 0
	for ind in range(num_games):
		max_streak = hot_streak(p, num_shots)
		if max_streak > streak_min:
			num_streaks += 1

	return num_streaks / num_games

def tries_until(p):
	tries = 0
	done = False
	while not done:
		val = rand.random()
		tries += 1
		if val < p:
			done = True

	return tries

def estimate_tries_until(p, num):
	tot_tries = 0
	for ind in range(num):
		tot_tries += tries_until(p)

	return tot_tries / num

def tries_until_both(p):
	tries = 0
	heads_done = False
	tails_done = False
	done = False
	while not done:
		val = rand.random()
		tries += 1
		if val < p:
			heads_done = True
		else:
			tails_done = True
		done = heads_done and tails_done

	return tries

def estimate_tries_until_both(p, num):
	tot_tries = 0
	for ind in range(num):
		tot_tries += tries_until_both(p)

	return tot_tries / num


def random_walk_1d(n):
	x = 0
	since_start = 0
	since_start_list = []
	for ind in range(n):
		val = rand.randint(0,1)
		val = val*2 - 1 # convert to +- 1
		x += val

		if x == 0:
			since_start_list.append(since_start)
			since_start = 0
		else:
			since_start += 1

	return np.array(since_start_list)

def random_walk_crossings_1d(n):
	distances = random_walk_1d(n)
	crossings = len(distances)
	crossings_per_step = crossings/n

	return crossings_per_step


def plot_rwc_spread(num_tries):
	nvec = np.array([10, 100, 1000, 10000, 100000])

	xvec = []
	yvec = []
	for n in nvec:
		for ind in range(num_tries):
			crossings_per_step = random_walk_crossings_1d(n)
			xvec.append(n)
			yvec.append(crossings_per_step)

	plt.figure(1)
	plt.clf()
	plt.grid(True)
	plt.xscale('log')
	plt.yscale('log')
	plt.ylim([1e-5, 1e0])
	plt.scatter(xvec, yvec)


def random_walk_2d(n):
	x = 0
	y = 0
	since_start = 0
	since_start_list = []
	for ind in range(n):
		val = rand.randint(0,3)
		if val <=1:
			val = val*2 - 1 # convert from 0, 1 to +- 1
			x += val
		else:
			val = (val-2)*2 - 1 # convert from 3, 4 to +- 1
			y += val

		if (x == 0) and (y == 0):
			since_start_list.append(since_start)
			since_start = 0
		else:
			since_start += 1

	return np.array(since_start_list)


def random_walk_3d(n):
	x = 0
	y = 0
	z = 0
	since_start = 0
	since_start_list = []
	for ind in range(n):
		val = rand.randint(0,5)
		if val <=1:
			val = val*2 - 1 # convert from 0, 1 to +- 1
			x += val
		elif val <= 3:
			val = (val-2)*2 - 1 # convert from 2, 3 to +- 1
			y += val
		else: # val = 4 or 5
			val = (val-4)*2 - 1 # convert from 4, 5 to +- 1
			z += val

		if (x == 0) and (y == 0) and (z == 0):
			since_start_list.append(since_start)
			since_start = 0
		else:
			since_start += 1

	return np.array(since_start_list)

def random_walk_crossings_2d(n):
	distances = random_walk_2d(n)
	crossings = len(distances)
	crossings_per_step = crossings/n

	return crossings_per_step

def random_walk_crossings_3d(n):
	distances = random_walk_3d(n)
	crossings = len(distances)
	crossings_per_step = crossings/n

	return crossings_per_step

def plot_rwc_spread_2d(num_tries):
	nvec = np.array([10, 100, 1000, 10000, 100000])

	xvec = []
	yvec = []
	for n in nvec:
		for ind in range(num_tries):
			crossings_per_step = random_walk_crossings_2d(n)
			xvec.append(n)
			yvec.append(crossings_per_step)

	plt.figure(1)
	plt.clf()
	plt.grid(True)
	plt.xscale('log')
	plt.yscale('log')
	plt.ylim([1e-6, 1e0])
	plt.scatter(xvec, yvec)


def plot_rwc_spread_all(num_tries=10, num_points=1e1):
	nn = np.logspace(0, 5, num_points)
	nvec = [int(el) for el in nn]
	
	alpha = 0.1
	plt.figure(1)
	plt.clf()
	plt.grid(True)

	xvec = []
	yvec = []
	for n in nvec:
		print(n)
		for ind in range(num_tries):
			crossings_per_step = random_walk_crossings_1d(n)
			xvec.append(n)
			yvec.append(crossings_per_step)
	plt.scatter(xvec, yvec, c='b', alpha=alpha)

	xvec = []
	yvec = []
	for n in nvec:
		print(n)
		for ind in range(num_tries):
			crossings_per_step = random_walk_crossings_2d(n)
			xvec.append(n)
			yvec.append(crossings_per_step)
	plt.scatter(xvec, yvec, c='r', alpha=alpha)
	
	xvec = []
	yvec = []
	for n in nvec:
		print(n)
		for ind in range(num_tries):
			crossings_per_step = random_walk_crossings_3d(n)
			xvec.append(n)
			yvec.append(crossings_per_step)
	plt.scatter(xvec, yvec, c='g', alpha=alpha)

	plt.xscale('log')
	plt.yscale('log')
	plt.ylim([1e-6, 1e0])
	plt.xlabel('Number of Steps')
	plt.ylabel('Return Fraction')
	plt.title('Estimating Random Walk Return Probabilities')
	plt.savefig('random_walk.svg', transparent=True)
	
	
def cloverleaf(r):
	P_exit_west = ((1-r)**2 * r) / (1-r**4)
	return P_exit_west

def cloverleaf_sweep(npts=1e3):
	# r is probability of turning right when you have a choice to turn or continue straight
	# assuming we're traveling northbound
	# Finds probability we exit traveling westbound
	r = np.linspace(0, 1, num=npts, endpoint=False)
	
	P_exit_west = ((1-r)**2 * r) / (1-r**4)
	
	plt.figure(1)
	plt.clf()
	plt.grid(True)
	plt.xlim((0, 1))
	plt.plot(r, P_exit_west)
	
