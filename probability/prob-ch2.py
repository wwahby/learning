import numpy as np
import random as rand
import matplotlib.pyplot as plt
import probscale as prb

def dart_cartesian():
	valid = False

	while not valid:
		x = rand.uniform(-1, 1)
		y = rand.uniform(-1, 1)

		r = x**2 + y**2

		valid = ( r <= 1)

	return (x,y)


def dart_radial():
	# don't need validity check since we're going to keep r in bounds

	r = rand.random()
	theta = 2*np.pi*rand.random()

	return (r, theta)


def cartesian_to_radial(x,y):
	r = x**2 + y**2

	theta = np.arctan(y/x)

	if x < 0:
		theta += np.pi

	return (r, theta)


def compare_methods(n=1e3):

	n = int(n)

	r_cartesian = np.zeros(n)
	r_radial = np.zeros(n)

	for ind in range(n):
		xy = dart_cartesian()
		cartesian_coords_conv = cartesian_to_radial(xy[0], xy[1])
		radial_coords = dart_radial()

		r_cartesian[ind] = cartesian_coords_conv[0]
		r_radial[ind] = radial_coords[0]


	plt.figure(1)
	plt.clf()
	ax = plt.gca()
	ax.grid(b=True, which='both')

	prb.probplot(r_cartesian, ax=ax, probax='y', color='b')
	prb.probplot(r_radial, ax=ax, probax='y', color='r')

	plt.xlabel('Radius')
	plt.ylabel('Percentile')


